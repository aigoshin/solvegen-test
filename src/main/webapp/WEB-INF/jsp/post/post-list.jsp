<%@include file="/WEB-INF/jsp/init.jsp" %>

<html>
<head>
    <title>Blog</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>
<body>

<div class="site-wrapper">
    <h2 align="center">Posts</h2>

    <form class="form-horizontal" role="form" action="<c:url value="/"/>" method="post">
        <div class="form-group ">
            <div class="col-lg-11">
                <input class="form-control " name="query" value="${query}">
            </div>
            <input type="submit" class="btn" value="Search">
        </div>
        <input type="hidden" name="action" value="getPosts">
    </form>

    </br>
    <a href="<c:url value="/?action=addPost"/>" class="btn btn-primary">Create post</a>

    <c:forEach items="${posts}" var="post">
        <div class="post-wrapper">
            <h4 align="center">${post.title}</h4>
            </br>
            <c:set var="tags" value="${fn:replace(post.tags,'[','')}"/>
            <c:set var="tags" value="${fn:replace(tags,']','')}"/>
            <div>Tags: ${tags}</div>
            <div>${post.description}</div>
            <div style="float:right"><fmt:formatDate pattern="dd-MM-yyyy" value="${post.createdDate}"/></div>

        </div>
        <div class="btn-group">
            <a href="<c:url value="/?action=addPost&id=${post.postId}"/>" class="btn btn-primary">Edit post</a>
            <a href="<c:url value="/?action=removePost&id=${post.postId}"/>" class="btn btn-danger">Delete post</a>
        </div>
    </c:forEach>
</div>

</div>
</body>
</html>