<%@include file="/WEB-INF/jsp/init.jsp" %>
<html>
<head>
    <title>Create|Edit Post</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
</head>

<body>
<div class="site-wrapper">
    <h2 align="center">Create|Edit Post</h2>

    <form class="form-horizontal" role="form" action="<c:url value="/"/>" method="post">


        <c:if test="${post==null}">
            <div class="form-group ">
                <label class="col-sm-2" for="title">Title</label>

                <div class="col-lg-10 ">
                    <input id="title" name="title" class="form-control col-lg-10" placeholder="Enter title">
                </div>
            </div>
        </c:if>

        <div class="form-group ">
            <label class="col-sm-2" for="tag">Tag list</label>

            <div class="col-lg-10 ">
                <c:set var="tags" value="${fn:replace(post.tags,'[','')}"/>
                <c:set var="tags" value="${fn:replace(tags,']','')}"/>
                <input id="tag" name="tag" class="form-control" placeholder='Enter tags with "," as separator' value="${tags}">
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-2" for="description">Description</label>

            <div class="col-lg-12">
                <textarea rows="10" id="description" class="form-control"
                          name="description">${post.description}</textarea>
            </div>
        </div>

        <input type="hidden" name="action" value="updatePost">
        <input type="hidden" name="title" value="${post.title}">
        <input type="hidden" name="id" value="${post.postId}">

        <div class="btn-group">
            <input type="submit" class="btn btn-primary" value="Save"/>
            <a href="<c:url value="/"/>" class="btn btn-default">Cancel</a>
        </div>
    </form>

</div>
</body>
</html>