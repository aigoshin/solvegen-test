<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<style>
    .site-wrapper {
        left: 50%;
        margin-left: -450px;
        padding-left: 15px;
        padding-right: 15px;
        position: absolute;
        width: 900px;
        height: 100%;
        background-color: #F5EDE3;
        border-left: 1px solid #BAA68E;
        border-right: 1px solid #BAA68E;
    }

    .post-wrapper {
        padding: 5px;
        margin-bottom: 15px;
        margin-top: 15px;
        background-color: #CDC5BB;
        border-left: 1px solid #BAA68E;
        border-right: 1px solid #BAA68E;
    }
</style>