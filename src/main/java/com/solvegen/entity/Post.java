package com.solvegen.entity;


import org.hibernate.search.annotations.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Entity
@Indexed
@Table(name = "post")
public class Post {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "postId")
    private Long postId;

    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    @Column(name = "title")
    private String title;

    @Lob
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    @Column(name = "description")
    private String description;

    @DateBridge(resolution = Resolution.DAY)
    @Field(index = Index.YES, analyze = Analyze.YES, store = Store.NO)
    @Column(name = "createdDate")
    private Date createdDate;


    @IndexedEmbedded
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "post_tag",
            joinColumns = {@JoinColumn(name = "postId", nullable = false)},
            inverseJoinColumns = {@JoinColumn(name = "tagId", nullable = false)})
    private List<Tag> tags = new ArrayList<>();

    public Long getPostId() {
        return postId;
    }

    public void setPostId(Long id) {
        this.postId = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
