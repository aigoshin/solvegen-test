package com.solvegen.dao.impl;


import com.solvegen.dao.BlogPostDao;
import com.solvegen.entity.Post;
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
import org.hibernate.search.query.dsl.QueryBuilder;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;


@Stateless
public class BlogPostDaoImpl implements BlogPostDao {

    @PersistenceContext(unitName = "blogUnit")
    private EntityManager em;

    @Override
    public List<Post> getAllPosts() {
        Query q = em.createQuery("select p from Post p");
        return q.getResultList();
    }

    @Override
    public List<Post> searchPost(String query) throws InterruptedException {
        FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(em);
        fullTextEntityManager.createIndexer().startAndWait();

        QueryBuilder builder = fullTextEntityManager.getSearchFactory().buildQueryBuilder().forEntity(Post.class).get();

        org.apache.lucene.search.Query luceneQuery = builder
                .keyword()
                .onFields("description", "title", "tags.tag")
                .matching(query)
                .createQuery();

        javax.persistence.Query jpaQuery = fullTextEntityManager.createFullTextQuery(luceneQuery, Post.class);
        return jpaQuery.getResultList();
    }

    @Override
    public Post savePost(Post post) {
        return em.merge(post);
    }

    @Override
    public void removePost(long id) {
        em.remove(get(id));
    }

    @Override
    public Post get(long id) {
        return em.find(Post.class, id);
    }

}
