package com.solvegen.dao;


import com.solvegen.entity.Post;

import javax.ejb.Local;
import java.util.List;

@Local
public interface BlogPostDao {

    public List<Post> getAllPosts();

    public List<Post> searchPost(String request) throws InterruptedException;

    public Post savePost(Post post);

    public void removePost(long id);

    public Post get(long id);

}
