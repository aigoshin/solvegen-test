package com.solvegen.service;


import com.solvegen.entity.Post;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Local
public interface BlogPostService {

    public List<Post> getAllPosts();

    public List<Post> searchPost(String query) throws InterruptedException;

    public Post savePost(Post post);

    public void removePost(long id);

    public Post getPost(long id);

    void bindPost(HttpServletRequest request);
}
