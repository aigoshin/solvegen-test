package com.solvegen.service.impl;


import com.solvegen.dao.BlogPostDao;
import com.solvegen.entity.Post;
import com.solvegen.entity.Tag;
import com.solvegen.service.BlogPostService;
import com.solvegen.util.ConverterUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@Stateless
public class BlogPostServiceImpl implements BlogPostService {

    @EJB
    private BlogPostDao blogPostDao;

    @Override
    public List<Post> getAllPosts() {
        return blogPostDao.getAllPosts();
    }

    @Override
    public List<Post> searchPost(String query) throws InterruptedException {
        return blogPostDao.searchPost(query);
    }

    @Override
    public Post savePost(Post post) {
        return blogPostDao.savePost(post);
    }

    @Override
    public void removePost(long id) {
        blogPostDao.removePost(id);
    }

    @Override
    public Post getPost(long id) {
        return blogPostDao.get(id);
    }

    @Override
    public void bindPost(HttpServletRequest request) {
        Post post = new Post();

        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String tags = request.getParameter("tag");

        Long id = ConverterUtils.toLong(request.getParameter("id"));

        post.setPostId(id);
        post.setTitle(title);
        post.setDescription(description);
        post.setCreatedDate(new Date());

        List<String> stringTagList = Arrays.asList(tags.split(","));
        List<Tag> tagList = new ArrayList<>();

        for (String tagString : stringTagList) {
            Tag tag = new Tag();
            tag.setTag(tagString);
            tagList.add(tag);
        }

        post.setTags(tagList);

        savePost(post);
    }

}
