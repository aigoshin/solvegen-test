package com.solvegen.servlet.strategy;

import com.solvegen.service.BlogPostService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public interface Dispatcher {

    public void doDispatch(HttpServletRequest request, HttpServletResponse response, BlogPostService service) throws Exception;
}