package com.solvegen.servlet.strategy.impl;


import com.solvegen.entity.Post;
import com.solvegen.service.BlogPostService;
import com.solvegen.servlet.strategy.Dispatcher;
import com.solvegen.util.ConverterUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AddBlogPost implements Dispatcher {

    @Override
    public void doDispatch(HttpServletRequest request, HttpServletResponse response, BlogPostService postService) throws Exception {
        Post post = null;
        Long id = ConverterUtils.toLong(request.getParameter("id"));

        if (id != null) {
            post = postService.getPost(id);
            request.setAttribute("post", post);
        }

        request.getRequestDispatcher("/WEB-INF/jsp/post/post-edit.jsp").forward(request, response);
    }
}
