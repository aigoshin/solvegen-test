package com.solvegen.servlet.strategy.impl;

import com.solvegen.entity.Post;
import com.solvegen.service.BlogPostService;
import com.solvegen.servlet.strategy.Dispatcher;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;


public class ListOfPosts implements Dispatcher {

    @Override
    public void doDispatch(HttpServletRequest request, HttpServletResponse response, BlogPostService postService) throws Exception {
        String query = request.getParameter("query");
        List<Post> posts;

        if (StringUtils.isEmpty(query)) {
            posts = postService.getAllPosts();
        } else {
            posts = postService.searchPost(query);
        }

        request.setAttribute("query", query);
        request.setAttribute("posts", posts);
        request.getRequestDispatcher("/WEB-INF/jsp/post/post-list.jsp").forward(request, response);
    }
}
