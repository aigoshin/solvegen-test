package com.solvegen.servlet.strategy.impl;


import com.solvegen.service.BlogPostService;
import com.solvegen.servlet.strategy.Dispatcher;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class UpdatePost implements Dispatcher {

    @Override
    public void doDispatch(HttpServletRequest request, HttpServletResponse response, BlogPostService postService) throws Exception {
        postService.bindPost(request);
        response.sendRedirect(request.getContextPath());
    }


}
