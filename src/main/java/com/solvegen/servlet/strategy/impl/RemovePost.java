package com.solvegen.servlet.strategy.impl;


import com.solvegen.service.BlogPostService;
import com.solvegen.servlet.strategy.Dispatcher;
import com.solvegen.util.ConverterUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class RemovePost implements Dispatcher {

    @Override
    public void doDispatch(HttpServletRequest request, HttpServletResponse response, BlogPostService postService) throws Exception {
        Long id = ConverterUtils.toLong(request.getParameter("id"));
        if (id != null) {
            postService.removePost(id);
        }
        response.sendRedirect(request.getContextPath());
    }
}
