package com.solvegen.servlet.strategy;


public class Action {

    private String action;

    public Action(String action) {
        action = action == null ? "getPosts" : action;
        this.action = action;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Action action1 = (Action) o;

        if (action != null ? !action.equals(action1.action) : action1.action != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return action != null ? action.hashCode() : 0;
    }
}
