package com.solvegen.servlet;


import com.solvegen.service.BlogPostService;
import com.solvegen.servlet.strategy.Action;
import com.solvegen.servlet.strategy.Dispatcher;
import com.solvegen.servlet.strategy.impl.*;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/")
public class BlogPostServlet extends HttpServlet {

    @EJB
    private BlogPostService service;

    public static final Map<Action, Dispatcher> ACTION_MAP = new HashMap<>();

    static {
        ACTION_MAP.put(new Action("addPost"), new AddBlogPost());
        ACTION_MAP.put(new Action("updatePost"), new UpdatePost());
        ACTION_MAP.put(new Action("getPosts"), new ListOfPosts());
        ACTION_MAP.put(new Action("removePost"), new RemovePost());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Dispatcher dispatcher = ACTION_MAP.get(new Action(req.getParameter("action")));
        try {
            dispatcher.doDispatch(req, resp, service);
        } catch (Exception e) {
            e.printStackTrace();
            req.setAttribute("error", e);
            req.getRequestDispatcher("/WEB-INF/jsp/error/error.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
